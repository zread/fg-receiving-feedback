﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 9/12/2016
 * Time: 2:56 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FGreceivingFeedBack
{
	partial class FormReports
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button BtDot;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.FolderBrowserDialog FBD;
		
		private System.Windows.Forms.TextBox TbPath;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReports));
			this.panel2 = new System.Windows.Forms.Panel();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.label2 = new System.Windows.Forms.Label();
			this.BtDot = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.FBD = new System.Windows.Forms.FolderBrowserDialog();
			this.TbPath = new System.Windows.Forms.TextBox();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dataGridView1);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Location = new System.Drawing.Point(51, 57);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1196, 621);
			this.panel2.TabIndex = 14;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(0, 24);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.Size = new System.Drawing.Size(1196, 597);
			this.dataGridView1.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label2.Dock = System.Windows.Forms.DockStyle.Top;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(1196, 24);
			this.label2.TabIndex = 14;
			this.label2.Text = "Carton Detail List";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// BtDot
			// 
			this.BtDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtDot.Location = new System.Drawing.Point(63, 698);
			this.BtDot.Name = "BtDot";
			this.BtDot.Size = new System.Drawing.Size(88, 23);
			this.BtDot.TabIndex = 17;
			this.BtDot.Text = "Export";
			this.BtDot.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.BtDot.UseVisualStyleBackColor = true;
			this.BtDot.Click += new System.EventHandler(this.BtDotClick);
			// 
			// TbPath
			// 
			this.TbPath.Location = new System.Drawing.Point(188, 699);
			this.TbPath.Name = "TbPath";
			this.TbPath.Size = new System.Drawing.Size(229, 20);
			this.TbPath.TabIndex = 18;
			// 
			// FormReports
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1298, 733);
			this.Controls.Add(this.TbPath);
			this.Controls.Add(this.BtDot);
			this.Controls.Add(this.panel2);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormReports";
			this.Text = "FormReports";
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
