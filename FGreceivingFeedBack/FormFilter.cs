﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/30/2016
 * Time: 11:58 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace FGreceivingFeedBack
{
	/// <summary>
	/// Description of FormFilter.
	/// </summary>
	public partial class FormFilter : Form
	{
		public FormFilter()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			TbCarton.Text = MainForm.DocHeader;
			TbMtrl.Text = MainForm.Mtrl;
			TbBatch.Text = MainForm.Batch;
			TbMvm.Text = MainForm.MvmType;	
			
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void BtAssignClick(object sender, EventArgs e)
		{
			MainForm.DocHeader = TbCarton.Text.Trim();
			MainForm.Mtrl = TbMtrl.Text.Trim();
			MainForm.Batch = TbBatch.Text.Trim();
			MainForm.MvmType = TbMvm.Text.Trim();
			
			this.Close();
		}
		void BtClearClick(object sender, EventArgs e)
		{
			TbCarton.Text = "";
			TbMtrl.Text = "";
			TbBatch.Text = "";
			TbMvm.Text = "";	
		}
	}
}
