﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/30/2016
 * Time: 11:58 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FGreceivingFeedBack
{
	partial class FormFilter
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox TbMtrl;
		private System.Windows.Forms.TextBox TbBatch;
		private System.Windows.Forms.TextBox TbMvm;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button BtAssign;
		private System.Windows.Forms.Button BtClear;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox TbCarton;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.TbMtrl = new System.Windows.Forms.TextBox();
			this.TbBatch = new System.Windows.Forms.TextBox();
			this.TbMvm = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.BtAssign = new System.Windows.Forms.Button();
			this.BtClear = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.TbCarton = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// TbMtrl
			// 
			this.TbMtrl.Location = new System.Drawing.Point(81, 57);
			this.TbMtrl.Name = "TbMtrl";
			this.TbMtrl.Size = new System.Drawing.Size(126, 20);
			this.TbMtrl.TabIndex = 1;
			// 
			// TbBatch
			// 
			this.TbBatch.Location = new System.Drawing.Point(81, 99);
			this.TbBatch.Name = "TbBatch";
			this.TbBatch.Size = new System.Drawing.Size(126, 20);
			this.TbBatch.TabIndex = 2;
			// 
			// TbMvm
			// 
			this.TbMvm.Location = new System.Drawing.Point(81, 139);
			this.TbMvm.Name = "TbMvm";
			this.TbMvm.Size = new System.Drawing.Size(126, 20);
			this.TbMvm.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 57);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 26);
			this.label1.TabIndex = 10;
			this.label1.Text = "Mtrl";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 93);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(51, 26);
			this.label2.TabIndex = 11;
			this.label2.Text = "Batch";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 139);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(63, 26);
			this.label3.TabIndex = 12;
			this.label3.Text = "MvmType";
			// 
			// BtAssign
			// 
			this.BtAssign.Location = new System.Drawing.Point(81, 187);
			this.BtAssign.Name = "BtAssign";
			this.BtAssign.Size = new System.Drawing.Size(89, 36);
			this.BtAssign.TabIndex = 4;
			this.BtAssign.Text = "Assign";
			this.BtAssign.UseVisualStyleBackColor = true;
			this.BtAssign.Click += new System.EventHandler(this.BtAssignClick);
			// 
			// BtClear
			// 
			this.BtClear.Location = new System.Drawing.Point(81, 229);
			this.BtClear.Name = "BtClear";
			this.BtClear.Size = new System.Drawing.Size(89, 36);
			this.BtClear.TabIndex = 7;
			this.BtClear.Text = "Clear";
			this.BtClear.UseVisualStyleBackColor = true;
			this.BtClear.Click += new System.EventHandler(this.BtClearClick);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(12, 21);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(51, 26);
			this.label4.TabIndex = 9;
			this.label4.Text = "Carton number";
			// 
			// TbCarton
			// 
			this.TbCarton.Location = new System.Drawing.Point(81, 21);
			this.TbCarton.Name = "TbCarton";
			this.TbCarton.Size = new System.Drawing.Size(126, 20);
			this.TbCarton.TabIndex = 0;
			// 
			// FormFilter
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(252, 275);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.TbCarton);
			this.Controls.Add(this.BtClear);
			this.Controls.Add(this.BtAssign);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.TbMvm);
			this.Controls.Add(this.TbBatch);
			this.Controls.Add(this.TbMtrl);
			this.Name = "FormFilter";
			this.Text = "FormFilter";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
