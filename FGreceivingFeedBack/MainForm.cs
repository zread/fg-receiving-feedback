﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/12/2016
 * Time: 3:22 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using MySql.Data.MySqlClient;
using System.Linq;

namespace FGreceivingFeedBack
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			showCover();
			
			TbPath.Text = ToolsClass.getConfig("ExcelPath",false,"","config.xml");
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		public static string DocHeader = "";
		public static string Mtrl = "";
		public static string Batch = "";
		public static string MvmType = "";
		
		
		 private void showCover()
        {
            FormCover f2 = new FormCover();
            f2.Owner = this;
            //f2.Size = this.Size;
            f2.ShowDialog();
   
        }
		void BtDotClick(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TbPath.Text)) 
			openFileDialog1.InitialDirectory = TbPath.Text.Substring(0,TbPath.Text.LastIndexOf('\\'));
			 openFileDialog1.ShowDialog();
			 openFileDialog1.Filter = "All File(*.*)|*.*";
             openFileDialog1.Title = "Select File to import";             
			 TbPath.Text = openFileDialog1.FileName;
			 ToolsClass.saveXMLNode("ExcelPath",TbPath.Text,false,"","config.xml");
		}
		void BtReadClick(object sender, EventArgs e)
		{
			if (!FormCover.CheckAccess("BtRead")) 				
				return;
			
			dataGridView1.DataSource = null;
			GetExcel();
		} 
		
		private void GetExcel()
	   	 {
		 	string fullPathToExcel = TbPath.Text.ToString();
		 	string connString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=yes'", fullPathToExcel);
		 	DataTable dt = new DataTable();

		 	try {	
		 		  using (OleDbConnection con = new OleDbConnection(connString))
				{    
					dt = GetDataTable(string.Format("SELECT * from [{0}]","Sheet1$"), connString);
					dataGridView1.DataSource = dt ;
				}	
		 	} 
		 	catch (Exception e) {
		 		MessageBox.Show(e.Message);
		 	} 
		 	
		 }
		
		static string[] GetExcelSheetNames(string connectionString)
		{
		        OleDbConnection con = null;
		        DataTable dt = null;
		        con= new OleDbConnection(connectionString);
		        con.Open();
		        dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
		
		        if (dt == null)
		        {
		            return null;
		        }
		
		        String[] excelSheetNames = new String[dt.Rows.Count];
		        int i = 0;
		
		        foreach (DataRow row in dt.Rows)
		        {
		            excelSheetNames[i] = row["TABLE_NAME"].ToString();
		            i++;
		        }
		
		        return excelSheetNames;
		}
		
		private DataTable GetDataTable(string sql, string connectionString)
        {
		 	DataTable dt = new DataTable();

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                using (OleDbCommand cmd = new OleDbCommand(sql, conn))
                {
                    using (OleDbDataReader rdr = cmd.ExecuteReader())
                    {
                        dt.Load(rdr);
                        return dt;
                    }
                }
            }
        }
		void BtImportClick(object sender, EventArgs e)
		{
			
			if (!FormCover.CheckAccess("BtImport")) 
				return;
			if(dataGridView1.Rows.Count < 1)
				return;	
			
			string datet = Convert.ToDateTime(dataGridView1.Rows[0].Cells[6].Value.ToString()).ToString("yyyy/MM/dd");
			string temp = "";
			switch (CbPeriod.SelectedIndex) {
				case 0:
					temp = datet.Substring(0,4);
					break;
				case 1:
					temp = datet.Substring(0,7);
					break;
				case 2:
					temp = datet;
					break;
				default:
					return;		
			}			
			
			
			string sql = @"delete from FGreceivingFeedBack where PostingDate like '{0}'";
				
				
				sql = string.Format(sql,"%"+temp+"%");
				ToolsClass.PutsData(sql);
			
				
			foreach (DataGridViewRow row in dataGridView1.Rows) {
				if (string.IsNullOrEmpty(row.Cells[0].Value.ToString()) ) {
					
					continue;
				}
					int temp1 = 0;
					
					
					
					string test = row.Cells[5].Value.ToString();
					
					if(!int.TryParse(test,out  temp1))
						continue;
				
					
					ListClass NewEntry = new ListClass();
					NewEntry.MovementType = row.Cells[0].Value.ToString();
					NewEntry.MaterialCode = row.Cells[1].Value.ToString();
					NewEntry.Sloc = row.Cells[2].Value.ToString();
					NewEntry.Batch = row.Cells[3].Value.ToString();
					NewEntry.LabelPower = row.Cells[4].Value.ToString();
					NewEntry.Quantity = Convert.ToInt32( row.Cells[5].Value.ToString());
					NewEntry.PostingDate = row.Cells[6].Value.ToString();
					NewEntry.MaterialDoc = row.Cells[7].Value.ToString();
					NewEntry.OrderNo = row.Cells[8].Value.ToString();
					NewEntry.DocumentHeader = row.Cells[9].Value.ToString();
					NewEntry.ReceivingType = row.Cells[10].Value.ToString();
						
						
					//Receiving Type
//					if(NewEntry.ReceivingType != "WF")
//						continue;
					
					if (string.IsNullOrEmpty(NewEntry.DocumentHeader)) 
						continue;
					
				datet = Convert.ToDateTime(NewEntry.PostingDate).ToString("yyyy/MM/dd");
				
				sql =@"insert into FGreceivingFeedBack ([MovementType]
							      ,[MaterialCode],[Sloc],[Batch],[LabelPower]
							      ,[Quantity],[PostingDate],[MaterialDoc],[OrderNo]
							      ,[DocumentHeader],[ProcessDateTime])
									values('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}','{8}','{9}',getdate())";
				sql = string.Format(sql,NewEntry.MovementType,NewEntry.MaterialCode,NewEntry.Sloc,
				                   NewEntry.Batch,NewEntry.LabelPower,NewEntry.Quantity,
				                    datet,NewEntry.MaterialDoc,NewEntry.OrderNo,
				                    NewEntry.DocumentHeader);
				ToolsClass.WriteToLog(sql,"");
				ToolsClass.PutsData(sql);
			}
		}
		void BtAnaClick(object sender, EventArgs e)
		{
			
			if (!FormCover.CheckAccess("BtAna")) 				
				return;
			
			panel3.BringToFront();
			string sql = @"select DocumentHeader,SAPReceiving,PackagingQty,MaterialCode,NumberEntry from (
						  select count(MaterialCode) as NumberEntry,sum(quantity) as SAPReceiving,MaterialCode,DocumentHeader from [FGreceivingFeedBack] where DocumentHeader like '%{0}%' and MaterialCode like '%{1}%' and batch like '%{2}%' and MovementType like '%{3}%' group by MaterialCode,DocumentHeader
						 ) a  left join
						 (
						 select * from  (
						 select count(module_sn) as PackagingQty,carton_no from TSAP_RECEIPT_UPLOAD_MODULE where Upload_Status <> 'Canceled' group by CARTON_NO
						 ) b ) c  on DocumentHeader = CARTON_NO where SAPReceiving <> PackagingQty and CARTON_NO not in (select Cartonno from CartonFixed)";
			DataTable dt = ToolsClass.PullData(string.Format(sql,DocHeader,Mtrl,Batch,MvmType));
			//dt.DefaultView.RowFilter = string.Format("MaterialCode LIKE '%{0}%'",Mtrl );
			
			dataGridView2.DataSource = dt;
			
			
		}
		void DataGridView2CellClick(object sender, DataGridViewCellEventArgs e)
		{
			//DataGridViewSelectedRowCollection theRowsSelected =.;
			if(e.RowIndex < 0)
				return;
			string cartonno = dataGridView2[1,e.RowIndex].Value.ToString();
			int rows = dataGridView2.Rows.Count;
			int column = dataGridView2.Columns.Count;
			//string cartonno = theRowsSelected[0].ToString();
			string sql = @"select * from FGreceivingFeedBack where documentheader = '"+ cartonno +"'";
			DataTable dt = ToolsClass.PullData(sql);
			
			dataGridView3.DataSource = dt;
			
		}
		
		
		void DataGridView2CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			
			if(e.RowIndex < 0)
				return;
			if(CbSafe.Checked == false)
				return;
		
			string cartonno = dataGridView2[1,e.RowIndex].Value.ToString();
	
			string sql = @"Update TSAP_RECEIPT_UPLOAD_MODULE set Upload_Status = 'Ready' where CARTON_NO = '"+ cartonno +"' and Upload_Status = 'Finished'"
				+ "delete from [SapUploadStatus] where Carton_no = '"+ cartonno +"'";
			ToolsClass.PutsData(sql);
			
			string message = "";
			DeleteFromESB(cartonno,out message);				
			CbSafe.Checked = false;
			
		}
		void BtExitClick(object sender, EventArgs e)
		{
			dataGridView2.DataSource = null;
			dataGridView3.DataSource = null;
			dataGridView1.DataSource = null;
			dataGridView4.DataSource = null;
			panel4.BringToFront();
		}
		
		
		
		
		 private int DeleteFromESB(string Carton,out string message)
        {
        	  
		 	string connection = ToolsClass.ESBDatabase();
        	string sql = " Delete from Stockin where carton_no = '"+Carton +"'";
	               sql = string.Format(sql,Carton);
			MySqlConnection con = new MySqlConnection(connection);
			MySqlCommand cmd = new MySqlCommand(sql, con);
				con.Open();
        	try
        	{
					int ret = cmd.ExecuteNonQuery();
					con.Close();
					message = "";
					return ret;
        	}
        	catch(MySql.Data.MySqlClient.MySqlException ex)
        	{
        		message = ex.Message;
        		return 0;
        		
        	} 
			finally
			{
				con.Close();
				con.Dispose();
			}
        
		}
		void BtFilterClick(object sender, EventArgs e)
		{
			if (!FormCover.CheckAccess("BtFilter")) 				
				return;
			
			FormFilter frm = new FormFilter();
			 frm.ShowDialog();
			  if (frm != null)
                frm.Dispose();
			BtAnaClick(sender,e);
		}
		void BtTempClick(object sender, EventArgs e)
		{
			panel4.BringToFront();
			string sql = @"select * from [AutoLinkTracebility] where ID > 40";
			DataTable dt = ToolsClass.PullData(sql);
			dataGridView4.DataSource = dt;
			
		}
		void DataGridView4CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if(e.RowIndex < 0)
				return;
			if (CbSafe2.Checked == false) 
				return;
			string Id = dataGridView4[0,e.RowIndex].Value.ToString();
			
			string sql = @"Update AutoLinkTracebility set Status = 0  where ID = {0} and Status = 1";
			
			ToolsClass.PutsData(string.Format(sql,Id));
			
		}
		void BtMGClick(object sender, EventArgs e)
		{
			if (!FormCover.CheckAccess("BtMG")) 				
				return;
			
			if(dataGridView2.DataSource == null)
				return;
			string [] Cartons = new string[dataGridView2.Rows.Count];		
			
			int  count = 0;
			
			foreach (DataGridViewRow row in dataGridView2.Rows) {
				if (Convert.ToBoolean(row.Cells[0].Value) == true) {
					
					Cartons[count] = row.Cells[1].Value.ToString();
					count ++;
				}
			}
			string sql = @"insert into CartonFixed values('{0}',getdate())";
			
			for (int i = 0; i < count; i++) {
				
				string sqlexe = string.Format(sql,Cartons[i].Trim());
				ToolsClass.PutsData(sqlexe);
			}
	
			BtAnaClick(sender,e);
			
		}
		void BtReportsClick(object sender, EventArgs e)
		{
				FormReports.Instance(this);
		}
	
		
		
		
		
		
		
		
			
	}
}
