﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/12/2016
 * Time: 3:22 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FGreceivingFeedBack
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox TbPath;
		private System.Windows.Forms.Button BtDot;
		private System.Windows.Forms.Button BtExit;
		private System.Windows.Forms.Button BtImport;
		private System.Windows.Forms.Button BtRead;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.DataGridView dataGridView2;
		private System.Windows.Forms.Button BtAna;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.DataGridView dataGridView3;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox CbPeriod;
		private System.Windows.Forms.CheckBox CbSafe;
		private System.Windows.Forms.Button BtFilter;
		private System.Windows.Forms.Button BtTemp;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.DataGridView dataGridView4;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox CbSafe2;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
		private System.Windows.Forms.Button BtMG;
		private System.Windows.Forms.Button BtReports;

		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.CbPeriod = new System.Windows.Forms.ComboBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.label1 = new System.Windows.Forms.Label();
			this.TbPath = new System.Windows.Forms.TextBox();
			this.BtDot = new System.Windows.Forms.Button();
			this.BtExit = new System.Windows.Forms.Button();
			this.BtImport = new System.Windows.Forms.Button();
			this.BtRead = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.dataGridView2 = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.BtAna = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.dataGridView3 = new System.Windows.Forms.DataGridView();
			this.label2 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.CbSafe = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.BtFilter = new System.Windows.Forms.Button();
			this.BtTemp = new System.Windows.Forms.Button();
			this.panel4 = new System.Windows.Forms.Panel();
			this.dataGridView4 = new System.Windows.Forms.DataGridView();
			this.CbSafe2 = new System.Windows.Forms.CheckBox();
			this.label4 = new System.Windows.Forms.Label();
			this.BtMG = new System.Windows.Forms.Button();
			this.BtReports = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.CbPeriod);
			this.panel1.Controls.Add(this.dataGridView1);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.TbPath);
			this.panel1.Controls.Add(this.BtDot);
			this.panel1.Location = new System.Drawing.Point(27, 53);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(517, 475);
			this.panel1.TabIndex = 7;
			// 
			// CbPeriod
			// 
			this.CbPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbPeriod.FormattingEnabled = true;
			this.CbPeriod.Items.AddRange(new object[] {
			"Yearly",
			"Monthly",
			"Daily"});
			this.CbPeriod.Location = new System.Drawing.Point(362, 13);
			this.CbPeriod.Name = "CbPeriod";
			this.CbPeriod.Size = new System.Drawing.Size(135, 21);
			this.CbPeriod.TabIndex = 15;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(0, 65);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(517, 410);
			this.dataGridView1.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(3, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "File Address";
			// 
			// TbPath
			// 
			this.TbPath.Location = new System.Drawing.Point(79, 19);
			this.TbPath.Name = "TbPath";
			this.TbPath.Size = new System.Drawing.Size(229, 20);
			this.TbPath.TabIndex = 0;
			// 
			// BtDot
			// 
			this.BtDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtDot.Location = new System.Drawing.Point(314, 13);
			this.BtDot.Name = "BtDot";
			this.BtDot.Size = new System.Drawing.Size(42, 26);
			this.BtDot.TabIndex = 2;
			this.BtDot.Text = "...";
			this.BtDot.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.BtDot.UseVisualStyleBackColor = true;
			this.BtDot.Click += new System.EventHandler(this.BtDotClick);
			// 
			// BtExit
			// 
			this.BtExit.Location = new System.Drawing.Point(605, 307);
			this.BtExit.Name = "BtExit";
			this.BtExit.Size = new System.Drawing.Size(79, 40);
			this.BtExit.TabIndex = 10;
			this.BtExit.Text = "Clear";
			this.BtExit.UseVisualStyleBackColor = true;
			this.BtExit.Click += new System.EventHandler(this.BtExitClick);
			// 
			// BtImport
			// 
			this.BtImport.Location = new System.Drawing.Point(605, 118);
			this.BtImport.Name = "BtImport";
			this.BtImport.Size = new System.Drawing.Size(79, 40);
			this.BtImport.TabIndex = 9;
			this.BtImport.Text = "Import";
			this.BtImport.UseVisualStyleBackColor = true;
			this.BtImport.Click += new System.EventHandler(this.BtImportClick);
			// 
			// BtRead
			// 
			this.BtRead.Location = new System.Drawing.Point(605, 53);
			this.BtRead.Name = "BtRead";
			this.BtRead.Size = new System.Drawing.Size(79, 40);
			this.BtRead.TabIndex = 8;
			this.BtRead.Text = "Read";
			this.BtRead.UseVisualStyleBackColor = true;
			this.BtRead.Click += new System.EventHandler(this.BtReadClick);
			// 
			// dataGridView2
			// 
			this.dataGridView2.AllowUserToAddRows = false;
			this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Column1});
			this.dataGridView2.Location = new System.Drawing.Point(0, 42);
			this.dataGridView2.Name = "dataGridView2";
			this.dataGridView2.Size = new System.Drawing.Size(517, 411);
			this.dataGridView2.TabIndex = 11;
			this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView2CellClick);
			this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView2CellContentClick);
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Column1";
			this.Column1.Name = "Column1";
			this.Column1.Width = 50;
			// 
			// BtAna
			// 
			this.BtAna.Location = new System.Drawing.Point(605, 187);
			this.BtAna.Name = "BtAna";
			this.BtAna.Size = new System.Drawing.Size(79, 40);
			this.BtAna.TabIndex = 12;
			this.BtAna.Text = "Analize";
			this.BtAna.UseVisualStyleBackColor = true;
			this.BtAna.Click += new System.EventHandler(this.BtAnaClick);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.dataGridView3);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Location = new System.Drawing.Point(27, 545);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1196, 144);
			this.panel2.TabIndex = 13;
			// 
			// dataGridView3
			// 
			this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView3.Location = new System.Drawing.Point(0, 24);
			this.dataGridView3.Name = "dataGridView3";
			this.dataGridView3.Size = new System.Drawing.Size(1196, 120);
			this.dataGridView3.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label2.Dock = System.Windows.Forms.DockStyle.Top;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(1196, 24);
			this.label2.TabIndex = 14;
			this.label2.Text = "Carton Detail List";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.dataGridView2);
			this.panel3.Controls.Add(this.CbSafe);
			this.panel3.Controls.Add(this.label3);
			this.panel3.Location = new System.Drawing.Point(706, 75);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(517, 453);
			this.panel3.TabIndex = 14;
			// 
			// CbSafe
			// 
			this.CbSafe.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.CbSafe.Location = new System.Drawing.Point(431, 2);
			this.CbSafe.Name = "CbSafe";
			this.CbSafe.Size = new System.Drawing.Size(83, 34);
			this.CbSafe.TabIndex = 15;
			this.CbSafe.Text = "Safty Lock";
			this.CbSafe.UseVisualStyleBackColor = false;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label3.Dock = System.Windows.Forms.DockStyle.Top;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(517, 39);
			this.label3.TabIndex = 15;
			this.label3.Text = "Issue Cartons";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// BtFilter
			// 
			this.BtFilter.Location = new System.Drawing.Point(605, 245);
			this.BtFilter.Name = "BtFilter";
			this.BtFilter.Size = new System.Drawing.Size(79, 40);
			this.BtFilter.TabIndex = 15;
			this.BtFilter.Text = "Filter";
			this.BtFilter.UseVisualStyleBackColor = true;
			this.BtFilter.Click += new System.EventHandler(this.BtFilterClick);
			// 
			// BtTemp
			// 
			this.BtTemp.Location = new System.Drawing.Point(605, 423);
			this.BtTemp.Name = "BtTemp";
			this.BtTemp.Size = new System.Drawing.Size(79, 40);
			this.BtTemp.TabIndex = 16;
			this.BtTemp.Text = "Template";
			this.BtTemp.UseVisualStyleBackColor = true;
			this.BtTemp.Click += new System.EventHandler(this.BtTempClick);
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.dataGridView4);
			this.panel4.Controls.Add(this.CbSafe2);
			this.panel4.Controls.Add(this.label4);
			this.panel4.Location = new System.Drawing.Point(706, 75);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(517, 453);
			this.panel4.TabIndex = 16;
			// 
			// dataGridView4
			// 
			this.dataGridView4.AllowUserToAddRows = false;
			this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView4.Location = new System.Drawing.Point(0, 42);
			this.dataGridView4.Name = "dataGridView4";
			this.dataGridView4.Size = new System.Drawing.Size(517, 411);
			this.dataGridView4.TabIndex = 11;
			this.dataGridView4.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView4CellContentClick);
			// 
			// CbSafe2
			// 
			this.CbSafe2.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.CbSafe2.Location = new System.Drawing.Point(431, 2);
			this.CbSafe2.Name = "CbSafe2";
			this.CbSafe2.Size = new System.Drawing.Size(83, 34);
			this.CbSafe2.TabIndex = 15;
			this.CbSafe2.Text = "Safty Lock";
			this.CbSafe2.UseVisualStyleBackColor = false;
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label4.Dock = System.Windows.Forms.DockStyle.Top;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(517, 39);
			this.label4.TabIndex = 15;
			this.label4.Text = "AutoTemplateLinking";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// BtMG
			// 
			this.BtMG.Location = new System.Drawing.Point(605, 362);
			this.BtMG.Name = "BtMG";
			this.BtMG.Size = new System.Drawing.Size(79, 40);
			this.BtMG.TabIndex = 17;
			this.BtMG.Text = "MarkGood";
			this.BtMG.UseVisualStyleBackColor = true;
			this.BtMG.Click += new System.EventHandler(this.BtMGClick);
			// 
			// BtReports
			// 
			this.BtReports.Location = new System.Drawing.Point(605, 488);
			this.BtReports.Name = "BtReports";
			this.BtReports.Size = new System.Drawing.Size(79, 40);
			this.BtReports.TabIndex = 18;
			this.BtReports.Text = "Reports";
			this.BtReports.UseVisualStyleBackColor = true;
			this.BtReports.Click += new System.EventHandler(this.BtReportsClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(1298, 733);
			this.Controls.Add(this.BtReports);
			this.Controls.Add(this.BtMG);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.BtTemp);
			this.Controls.Add(this.BtFilter);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.BtAna);
			this.Controls.Add(this.BtExit);
			this.Controls.Add(this.BtImport);
			this.Controls.Add(this.BtRead);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel3);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Text = "FGreceivingFeedBack";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
			this.panel3.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
			this.ResumeLayout(false);

		}
		}
	}

