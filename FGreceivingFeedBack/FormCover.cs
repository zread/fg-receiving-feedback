﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 9/12/2016
 * Time: 1:33 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace FGreceivingFeedBack
{
	/// <summary>
	/// Description of FormCover.
	/// </summary>
	public partial class FormCover : Form
	{
		public static int UserAccess = -1;
		public static string UserName = "";
		private static List<string> allRight = new List<string>();
		public FormCover()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			textBox1.Focus();	
			textBox1.Select();
			
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		   
		void BtExitClick(object sender, EventArgs e)
		{
			 System.Environment.Exit(0);
		}
		void LoginClick(object sender, EventArgs e)
		{
			string sql = @"select [UserGroup],[UserNM] from UserLogin where UserNM = '{0}' and UserPW = '{1}'";
			sql = string.Format(sql,textBox1.Text,textBox2.Text);
			DataTable dt = ToolsClass.PullData(sql);
			if(dt == null || dt.Rows.Count < 1)
			{
				MessageBox.Show("User login or Password incorrect. Please verify!","Login Failed",MessageBoxButtons.OK,MessageBoxIcon.Error);
				return;
			}
			UserAccess = Convert.ToInt16( dt.Rows[0][0].ToString());
			UserName = dt.Rows[0][1].ToString();
			
			
			switch (UserAccess) {
				case 9:
					allRight.Add("BtRead");
					allRight.Add("BtImport");
					allRight.Add("BtAna");
					allRight.Add("BtFilter");
					allRight.Add("BtMG");
					allRight.Add("BtTemp");
					break;
				case 10:
					allRight.Add("BtRead");
					allRight.Add("BtImport");
					break;
				case 13:
					allRight.Add("BtAna");
					allRight.Add("BtFilter");
					break;
				default:
					allRight.Add("BtTemp");
					break;
			}
			Close();
			
		}
		protected override void OnFormClosing(FormClosingEventArgs e)
		{
		    base.OnFormClosing(e);
		    if (UserName == "") {
		    	
		    	System.Environment.Exit(0);
		    }
		}
		 private void textBox_Enter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.Color.Yellow;
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.SystemColors.Window;
        }
		public static bool CheckAccess(string Module)
		{
			if (allRight.Contains(Module)) {
				return true;
			}
			else
			{
				MessageBox.Show("User do not have enough privilege!");
				return false;
			}
		}
		void Textbox2_KeyPress(object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == '\r')
			{
				Login.Focus();
				LoginClick(null,null);
			}
			
			
		}
		
	}
}
