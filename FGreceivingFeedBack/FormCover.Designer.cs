﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 9/12/2016
 * Time: 1:33 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace FGreceivingFeedBack
{
	partial class FormCover
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button Login;
		private System.Windows.Forms.Button BtExit;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCover));
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.Login = new System.Windows.Forms.Button();
			this.BtExit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(113, 116);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(118, 20);
			this.textBox2.TabIndex = 11;
			this.textBox2.UseSystemPasswordChar = true;
			this.textBox2.Enter += new System.EventHandler(this.textBox_Enter);
			this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Textbox2_KeyPress);
			this.textBox2.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(113, 77);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(118, 20);
			this.textBox1.TabIndex = 10;
			this.textBox1.Enter += new System.EventHandler(this.textBox_Enter);
			this.textBox1.Leave += new System.EventHandler(this.textBox_Leave);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(26, 115);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(81, 26);
			this.label2.TabIndex = 9;
			this.label2.Text = "Password:";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(26, 76);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(81, 26);
			this.label1.TabIndex = 8;
			this.label1.Text = "Username:";
			// 
			// Login
			// 
			this.Login.Location = new System.Drawing.Point(44, 159);
			this.Login.Name = "Login";
			this.Login.Size = new System.Drawing.Size(73, 28);
			this.Login.TabIndex = 7;
			this.Login.Text = "Login";
			this.Login.UseVisualStyleBackColor = true;
			this.Login.Click += new System.EventHandler(this.LoginClick);
			// 
			// BtExit
			// 
			this.BtExit.Location = new System.Drawing.Point(145, 159);
			this.BtExit.Name = "BtExit";
			this.BtExit.Size = new System.Drawing.Size(73, 28);
			this.BtExit.TabIndex = 12;
			this.BtExit.Text = "Exit";
			this.BtExit.UseVisualStyleBackColor = true;
			this.BtExit.Click += new System.EventHandler(this.BtExitClick);
			// 
			// FormCover
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(280, 219);
			this.Controls.Add(this.BtExit);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Login);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormCover";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "FormCover";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
