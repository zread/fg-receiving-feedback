﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 9/12/2016
 * Time: 2:56 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel; 


namespace FGreceivingFeedBack
{
	/// <summary>
	/// Description of FormReports.
	/// </summary>
	public partial class FormReports : Form
	{
		public FormReports()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			showReport();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		private static FormReports theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormReports();
                //theSingleton.MdiParent = fm;
                //theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Size = fm.Size;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		private void showReport()
		{
			
			string sql = @" select * from [FGreceivingFeedBack] where DocumentHeader like '%{0}%' and MaterialCode like '%{1}%' and batch like '%{2}%' and MovementType like '%{3}%' ";
			DataTable dt = ToolsClass.PullData(string.Format(sql,MainForm.DocHeader,MainForm.Mtrl,MainForm.Batch,MainForm.MvmType));
			dataGridView1.DataSource = dt;
		}
		void BtDotClick(object sender, EventArgs e)
		{
			 
			
		     SaveFileDialog saveFileDialog1 = new SaveFileDialog();
		     saveFileDialog1.InitialDirectory = Application.StartupPath;
		     saveFileDialog1.Filter = "Excel 2007-2013 files (*.xlsx)|*.xlsx|txt files (*.txt)|*.txt|All files (*.*)|*.*"  ;
		     saveFileDialog1.FilterIndex = 2 ;
		     saveFileDialog1.RestoreDirectory = true ;
		
		     if(saveFileDialog1.ShowDialog() == DialogResult.OK)
		     {
		     	 ExportToExcel2(saveFileDialog1.FileName);
		     }
		     
		     TbPath.Text =  saveFileDialog1.FileName;
		     
			 ToolsClass.saveXMLNode("Saving Path",TbPath.Text,false,"","config.xml");
			 
		}
		
	
		
		void ExportToExcel2(string FileName)
		{
			 Microsoft.Office.Interop.Excel._Application app  = new Microsoft.Office.Interop.Excel.Application();
 
 
            // creating new WorkBook within Excel application
            Microsoft.Office.Interop.Excel._Workbook workbook =  app.Workbooks.Add(Type.Missing);
           
 
            // creating new Excelsheet in workbook
             Microsoft.Office.Interop.Excel._Worksheet worksheet = null;                   
           
           // see the excel sheet behind the program
            app.Visible = false;
            app.DisplayAlerts = false;
          
           // get the reference of first sheet. By default its name is Sheet1.
           // store its reference to worksheet
         
            worksheet = (Excel.Worksheet)workbook.Sheets["Sheet1"];
            worksheet = (Excel.Worksheet) workbook.ActiveSheet;
 
            // changing the name of active sheet
            worksheet.Name = "Exported FG uploading";
 
           
            // storing header part in Excel
            for(int i=1;i<dataGridView1.Columns.Count+1;i++)
            {
   				 worksheet.Cells[1, i] = dataGridView1.Columns[i-1].HeaderText;
            }
 
 
 
            // storing Each row and column value to excel sheet
            for (int i=0; i < dataGridView1.Rows.Count-1 ; i++)
            {
                for(int j=0;j<dataGridView1.Columns.Count;j++)
                {
                    worksheet.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                }
            }
 
 
            // save the application
            workbook.SaveAs(FileName,Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive , Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            MessageBox.Show("File saved successfully!","Message",MessageBoxButtons.OK,MessageBoxIcon.Information);
            // Exit from the application
          app.Quit();
        }
		
		
	}
}
