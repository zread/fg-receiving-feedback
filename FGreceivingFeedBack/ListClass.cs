﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/25/2016
 * Time: 10:54 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
namespace FGreceivingFeedBack
{
	/// <summary>
	/// Description of ListClass.
	/// </summary>
	public partial class ListClass 
	{
		public ListClass()
		{
		
		}
		private	string _MovementType = "";
		private string _MaterialCode = "";
		private string _Sloc = "";
		private string _Batch = "";
		private string _LabelPower = "";
		private int _Quantity = 0;
		private string _PostingDate = "";
		private string _MaterialDoc = "";
		private string _OrderNo = "";
		private string _DocumentHeader = "";
		private string _ReceivingType = "";
	
		
		public string MovementType
		{
			get { return _MovementType; }
			set	{ _MovementType = value; }			
		}
		public string MaterialCode
		{
			get { return _MaterialCode; }
			set	{ _MaterialCode = value; }			
		}
		public string Sloc
		{
			get { return _Sloc; }
			set	{ _Sloc = value; }			
		}
		public string Batch
		{
			get { return _Batch; }
			set	{ _Batch = value; }			
		}
		public string LabelPower
		{
			get { return _LabelPower; }
			set	{ _LabelPower = value; }			
		}public int Quantity
		{
			get { return _Quantity; }
			set	{ _Quantity = value; }			
		}
		public string PostingDate
		{
			get { return _PostingDate; }
			set	{ _PostingDate = value; }			
		}
		public string MaterialDoc
		{
			get { return _MaterialDoc; }
			set	{ _MaterialDoc = value; }			
		}
		
		
		
		
		public string OrderNo
		{
			get { return _OrderNo; }
			set	{ _OrderNo = value; }			
		}
		public string DocumentHeader
		{
			get { return _DocumentHeader; }
			set	{ _DocumentHeader = value; }			
		}	
		public string ReceivingType
		{
			get { return _ReceivingType; }
			set { _ReceivingType = value; }
		}
	
	
	}
}
