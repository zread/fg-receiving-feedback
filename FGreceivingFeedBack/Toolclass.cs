﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/12/2016
 * Time: 3:28 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Net.Mail;

namespace FGreceivingFeedBack
{
	/// <summary>
	/// Description of Toolclass.
	/// </summary>
public partial class ToolsClass : Form
	{
		public ToolsClass()
		{
			
		}
		public const int iSerial_Length = 4;
        public const int iDate_Length = 3;
        private static object lockGenerateBoxID;
        
		public static string GetConnection()
		{
			string DataSource = ToolsClass.getConfig ("DataSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("UserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("Password",false,"","Config.xml");	
			string DBName = ToolsClass.getConfig ("DBName",false,"","Config.xml");	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		public static string GetK3Connection()
		{
			string DataSource = ToolsClass.getConfig ("DataSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("K3UserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("K3Password",false,"","Config.xml");	
			string DBName = ToolsClass.getConfig ("K3DBName",false,"","Config.xml");	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		public static string csimes_SapInterface()
		{
			string DataSource = ToolsClass.getConfig ("DataSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("UserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("Password",false,"","Config.xml");	
			string DBName = "csimes_SapInterface";
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		public static string CentralizedDatabase()
		{
			string DataSource = ToolsClass.getConfig ("DataSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("UserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("Password",false,"","Config.xml");	
			string DBName = "CentralizedDatabase";	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		public static string ESBDatabase()
		{
			string DataSource = ToolsClass.getConfig ("ESBSource",false,"","Config.xml");	
			string UserID = ToolsClass.getConfig ("ESBUserID",false,"","Config.xml");	
			string Password = ToolsClass.getConfig ("ESBPassword",false,"","Config.xml");	
			string DBName = ToolsClass.getConfig ("ESBDBName",false,"","Config.xml");	;	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		
		
		 public static SqlDataReader GetDataReader(string SqlStr,string sDBConn="")
        {
            SqlDataReader dr = null;
            string ConnStr = "";
            if(string.IsNullOrEmpty(sDBConn))
            	ConnStr =  GetConnection();
            else
            ConnStr = sDBConn;
         
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                     dr = cmd.ExecuteReader();//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dr = null;
                }
               
            }
            return dr;
        }

		 public static DataTable PullData(string query,string connString = "")
    	{
		 	if(string.IsNullOrEmpty(connString))
		 		connString = GetConnection();
		 	
		 	try{
		    		DataTable dt = new DataTable();
        			SqlConnection conn = new SqlConnection(connString);        
      				SqlCommand cmd = new SqlCommand(query, conn);
       				conn.Open();

       				 // create data adapter
        			SqlDataAdapter da = new SqlDataAdapter(cmd);
        			// this will query your database and return the result to your datatable
       			 	da.Fill(dt);
        			conn.Close();
        			da.Dispose();
        			return dt;
		    	}
		    	catch (Exception e)
		    	{
		    		throw e;							    		
		    	}
		    
		
    	}
		 public static int PutsData(string query,string connString = "")
   		{
		    if(string.IsNullOrEmpty(connString))
		 		connString = GetConnection();		
		 	try{
		    		SqlConnection conn = new SqlConnection(connString);
		    		conn.Open();
   					SqlCommand myCommand = new SqlCommand(query,conn);

   					int i = myCommand.ExecuteNonQuery();   					
   					conn.Close();
   					return i;
		    	}
		    	catch (Exception ex)
		    	{
		    		System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
		    		return 0;								    	
		    	}
    	}
		 public static void saveXMLNode(string snode, string value = "", bool isAttribute = false, string sAttributeName = "",string fileName="")
        {
            string xmlFile = "";
            string ParentNode = "";
            if (fileName == "" || fileName == null)
            {
                xmlFile = Application.StartupPath + "\\config.xml";
                ParentNode = "config";
            }
            else
            {
                xmlFile = Application.StartupPath + "\\" + fileName;
                ParentNode = "config";
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile);
            XmlNodeList nodeList = xmlDoc.SelectSingleNode(ParentNode).ChildNodes;
            foreach (XmlNode xn in nodeList)
            {
                XmlElement xe = (XmlElement)xn;
                if (xe.Name == snode)
                {
                    if (isAttribute)
                    {
                        xe.SetAttribute(sAttributeName, value);
                        break;
                    }
                    else
                    {
                        xe.InnerText = value;
                        break;
                    }
                }
            }
            try
            {
                xmlDoc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
		 
		 
		 public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "config.xml";
            string xmlFile = AppDomain.CurrentDomain.BaseDirectory + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {    
            	return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
              	System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            	return "";
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }
		 

		 

		  public static void WritetoFile(string C,string template, string FilePath = "")
		  {
		  	if(FilePath == "")
		  		FilePath = Application.StartupPath + "\\Log.txt";
		  	
		  	File.AppendAllText(FilePath, Environment.NewLine + C + "	 " + template + "	 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
		  }
		  
		  public static void WriteErrorLog(string C,string template,string error)
		  {
//		  	string FilePath = Application.StartupPath + "\\ErrorLog.txt";
//		  	File.AppendAllText(FilePath, Environment.NewLine + C + "	 " + template + "	 " + error+ "	 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));		  
		  		StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + C + "	" + template + "	" + error);
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
		  
		  }
		  
	        public static void WriteToLog(string C,string template)
	        {
	            StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory +"\\Log.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + C + "	" + template);
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
	        }
		  
		  public static int GetQtyLeftInTemplate(string PO)
        {

			string sql = @"Select [RESV07] as Stock,[RESV08] as Pend from [csimes_SapInterface].[dbo].[T_WORK_ORDERS] where TWO_NO = '"+ PO +"'";
			SqlDataReader rd = ToolsClass.GetDataReader(sql,csimes_SapInterface());
			int Stock = 0, Pend = 0;
        	if(rd!= null&&rd.Read())
        	{
      			Stock = Convert.ToInt32(rd.GetValue(0));
      			Pend  = Convert.ToInt32(rd.GetValue(1));
       			rd.Close();
        	}
        	if (Stock > Pend) return 0;       		
        	
        	return Pend - Stock;        	
        }
		      
		public static int AddSNandMaterialTemplateLink(string TemplateSysID, string SN)
        {
            int returnValue = 0;
            SqlConnection sqlConne = new SqlConnection(csimes_SapInterface());
            sqlConne.Open();
            SqlTransaction sqltran = sqlConne.BeginTransaction();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            cmd.Transaction = sqltran;

            try
            {
                    string strSqlInsert = @"INSERT INTO [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                        ([MODULE_SN],[PACK_MA_SYSID],[LASTUPDATETIME],[LASTUPDATEUSER])
                        VALUES('{0}','{1}','{2}','{3}')";
                    strSqlInsert = string.Format(strSqlInsert, SN, TemplateSysID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), "SYSTEM");

                    cmd.CommandText = strSqlInsert;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        returnValue = returnValue + 1;
                    }
             		sqltran.Commit();
                return returnValue;
            }
            catch
            {
                returnValue = 0;
                sqltran.Rollback();
                return returnValue;
            }
            finally
            {
                sqlConne.Close();
                sqltran.Dispose();
                sqlConne.Dispose();
            }
        }
		      
		public static int UpdateSNandMaterialTemplateLink(string TemplateSysID, string SN)
        {
            int returnValue = 0;
            SqlConnection sqlConne = new SqlConnection(csimes_SapInterface());
            sqlConne.Open();
            SqlTransaction sqltran = sqlConne.BeginTransaction();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConne;
            cmd.Transaction = sqltran;

  
            try
            {
            	
                    string strSqlUpdate = @"UPDATE [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK]
                                        SET [PACK_MA_SYSID] = '{0}',[LASTUPDATETIME]='{1}',[LASTUPDATEUSER]='{2}'
                                        WHERE [MODULE_SN] = '{3}'";
                    strSqlUpdate = string.Format(strSqlUpdate, TemplateSysID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), "SYSTEM", SN);

                    cmd.CommandText = strSqlUpdate;
                    if (cmd.ExecuteNonQuery() == 1)
                    {
                        returnValue = returnValue + 1;
                    }
                sqltran.Commit();
                return returnValue;
            }
            catch
            {
                returnValue = 0;
                sqltran.Rollback();
                return returnValue;
            }
            finally
            {
                sqlConne.Close();
                sqltran.Dispose();
                sqlConne.Dispose();
            }


        }
      
		   
		public static void SendEmail(string body)
        {
           	
        	try
            {
        		string host = getConfig("smtphost",false,"","Config.xml");
        		string port =  getConfig("smtpport",false,"","Config.xml");
        		string MailFrom = getConfig("EmailFrom",false,"","Config.xml");
        		string MailTo = getConfig("EmailTo",false,"","Config.xml");
        		string subject = getConfig("Subject",false,"","Config.xml");
        		string userNM = getConfig("userNM",false,"","Config.xml");
        		string PsW = getConfig("PassWord",false,"","Config.xml");
        		string[] Receipts = MailTo.Split(';');
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(host);

                mail.From = new MailAddress(MailFrom);
                foreach (string r in Receipts)
                	mail.To.Add(r);
                mail.Subject = subject;
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(port);   
               	SmtpServer.Credentials = new System.Net.NetworkCredential(userNM, PsW);          
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        } 
		public static void MakeTransaction(string Query,string connString = "")
		{
			string connectionString = "";
			if(string.IsNullOrEmpty(connString))
		 		connectionString = GetConnection();
			else
				connectionString = connString;
			using (SqlConnection connection = new SqlConnection(connectionString))
		    {
		        connection.Open();
		
		        SqlCommand command = connection.CreateCommand();
		        SqlTransaction transaction;
		
		        // Start a local transaction.
		        transaction = connection.BeginTransaction("SampleTransaction");		
		        // Must assign both transaction object and connection
		        // to Command object for a pending local transaction
		        command.Connection = connection;
		        command.Transaction = transaction;
		
		        try
		        {
		            command.CommandText = Query;
		            command.ExecuteNonQuery();
		            // Attempt to commit the transaction.
		            transaction.Commit();
		            MessageBox.Show("Updating success.","Message",MessageBoxButtons.OK,MessageBoxIcon.Information);
		            WriteToLog(Query,"Success");
		        }
		        catch (Exception ex)
		        {
		        	MessageBox.Show(string.Format("Commit Exception Type: {0},  Message: {1}",ex.GetType(),ex.Message),"Message",MessageBoxButtons.OK,MessageBoxIcon.Information);
		        	WriteErrorLog(string.Format("Commit Exception Type: {0},  Message: {1}",ex.GetType(),ex.Message),"",Query);
		        	// Attempt to roll back the transaction.
		            try
		            {
		                transaction.Rollback();
		                WriteErrorLog("rolling back transection",ex.Message,Query);
		            }
		            catch (Exception ex2)
		            {
		            	WriteErrorLog("Error during rolling back transection",ex2.Message,Query);
		            }
		        }
		    }
		}
		  public static string getBoxID(string sYear="",string sDayOfYear="")
        {
            string sboxid = "";
            string sStationNo = "45"; //rework
            string sCellColor = "2"; //Poly
            string strsql="select * from Box where BoxID='{0}' and (IsUsed<>'N' OR number > 0)";
            //string strsql="select * from Box where BoxID='{0}'";
            
            bool isUse = true;
         
            int iSerial = int.Parse(ToolsClass.getConfig("SerialCode"));
//            try
//            {
//                DateTime dt = Convert.ToDateTime(getConfig("SystemDate"));
//                if (DateDiff(DateInterval.Month, dt, DateTime.Now.Date) > 0)
//                {
//                    saveXMLNode("SystemDate", DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss"));
//                    iSerial = 1;
//                }
//            }
//            catch(Exception e)
//            {
//                MessageBox.Show("Get System Date Fail:"+e.Message, "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
//            }

            lockGenerateBoxID = new object();
            if(sYear.Equals(""))
                sYear = DateTime.Now.Year.ToString().Substring(2,2);
            if (sDayOfYear.Equals(""))
                sDayOfYear = DateTime.Now.DayOfYear.ToString().PadLeft(iDate_Length, '0');
            string sMonth = DateTime.Now.Month.ToString().PadLeft(2,'0');

            while (isUse)
            {
                if (iSerial > 9999)
                {
                    MessageBox.Show("Carton Serial Number Has Run Out!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return "";
                }
                sboxid = sYear + sStationNo + sCellColor + sMonth + "0" + iSerial.ToString().PadLeft(iSerial_Length, '0');
                
                DataTable dt = PullData(string.Format(strsql, sboxid));                
                if ( dt!= null && dt.Rows.Count > 0)
                    isUse = true;
                else
                    isUse = false;
                iSerial++;
            }

            //saveXMLNode("SerialCode", iSerial.ToString());
            lock (lockGenerateBoxID)//Lock
            {
                saveBoxCodeList(sboxid);
                strsql = "update Box set IsUsed='W' where BoxID='" + sboxid + "'";
                PutsData(strsql);
            }
            return sboxid;
        }
        public static void saveBoxCodeList(string sBoxID)//(CheckedListBox clb)
        {
            int line = 0;
            string sql = "";
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(GetConnection()))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                sql="if not exists (select BoxID from Box where boxid='" + sBoxID + "') insert into Box(BoxID,Number,BoxType,ModelType) values('" + sBoxID + "',0,'','') ";
                //sql = sql + "else update Box set IsUsed='W' where boxid='" + sBoxID + "'";//add by alex.dong|2012-03-15
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    line = cmd.ExecuteNonQuery();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    //Log--BoxID Repeat,do nothing
                    conn.Close();
                }
            }
        }
		
		public static String GetTimestamp(DateTime value)
        {
            return value.ToString("dd'/'MM'/'yy HH:mm:ss:ffff");
        }
		
		
	}
}
